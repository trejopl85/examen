# Examen

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

## Install dependencies

Run `npm install` to download the dependencies

## Deplloying on Firebase

Run ` npm install -g firebase-tools `if you have not installed FireBase cli

Run `firebase login` to login in your firebase account

Run ` ng build --prod ` to build the app

Run `firebase init` select the project from the list or configured it in the .firebaserc file.

## Create a folder with the name as public and copy the files in dist folder and paste in public folder

What do you want to use as your public directory? public

File dist/ci-cd-app/index.html already exists. Overwrite? (y/N) N

Run `firebase deploy` to deploy the application

## Note: Sometimes the service: http://dummy.restapiexample.com/api/v1/employees responds an HTTP 400 error
## If this happens please reload the page and try again
## this is important because if the service failed, the website looks in blank

## You cant check the site at: https://{your-firebase-proj-id}.firebaseapp.com/

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

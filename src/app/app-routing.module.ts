import { EmpleadoResolverService } from './core/resolver/empelado-resolver/empleado-resolver.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaEmpleadoComponent } from './home/empleado/lista-empleado/lista-empleado.component';

const routes: Routes = [
  {
    path: '',
    component: ListaEmpleadoComponent,
    resolve: [
      EmpleadoResolverService
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [
    EmpleadoResolverService
  ]

})
export class AppRoutingModule { }

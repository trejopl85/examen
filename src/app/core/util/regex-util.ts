export const  REGEX = Object.freeze({
  LETTERS_AND_CHARACTERS: /^[A-Za-z .áéíóúÁÉÍÓÚñÑ äëïöüÄËÏÖÜñÑ]+$/,
  NUMBERS_DECIMAL: /^[0-9.]+$/,
  ONLY_NUMERS:/^[0-9]+$/,
});

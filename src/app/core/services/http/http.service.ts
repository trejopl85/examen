import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }
  /**
   * Funcion que realiza la peticion GET al backend
   * Si no puede completar la operacion la reintenta
   * @param urlGet la url a la que se hace la peticion
   * @param data payload a enviar al backend
   * @returns la respuesta del backend
   */
  getGenerico<T>(urlGet: string, data?: {}): Observable<HttpResponse<T>> {
    return this.http
      .get<T>(urlGet, {
        observe: 'response',
        params: data,
      }).pipe(retry(1));
  }
}

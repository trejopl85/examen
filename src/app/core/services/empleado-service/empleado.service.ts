import { HttpService } from './../http/http.service';
import { Injectable } from '@angular/core';
import { EmpleadoModel } from 'src/app/shared/model/EmpleadoModel';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  private _empleados: EmpleadoModel[] = [];

  constructor(private _http: HttpService) { }

  get http(): HttpService {
    return this._http;
  }
  set empleados(empleados: EmpleadoModel[]) {
    this._empleados = empleados;
  }
  get empleados(): EmpleadoModel[] {
    return this._empleados;
  }
}

import { AlertConfiguration } from './../../../shared/alert-module/alert/alert.component';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  isLoading = new Subject<boolean>();
  private _alertConfiguration: AlertConfiguration = {
    message: 'Exito',
    type: 'success'
  };
  constructor() {
  }
  set alertConfiguration(alert: AlertConfiguration) {
    this._alertConfiguration = alert;
  }
  get alertConfiguration(): AlertConfiguration {
    return this._alertConfiguration;
  }
  show() {
    this.isLoading.next(true);
  }
  hide() {
    this.isLoading.next(false);
  }
}

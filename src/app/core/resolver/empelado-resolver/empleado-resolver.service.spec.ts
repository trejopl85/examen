import { TestBed } from '@angular/core/testing';

import { EmpleadoResolverService } from './empleado-resolver.service';

describe('EmpleadoResolverService', () => {
  let service: EmpleadoResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmpleadoResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

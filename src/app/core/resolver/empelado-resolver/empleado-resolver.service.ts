import * as _ from 'lodash';
import { EmpleadoModel } from './../../../shared/model/EmpleadoModel';
import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EmpleadoService } from '../../services/empleado-service/empleado.service';
import { AlertService } from '../../services/alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoResolverService implements Resolve<EmpleadoModel> {
  private uri = 'https://dummy.restapiexample.com/api/v1/employees';
  constructor(private empleadoService: EmpleadoService, private alertService: AlertService) {

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<EmpleadoModel> | any | boolean {
    return this.empleadoService.http.getGenerico<EmpleadoModel[]>(this.uri).toPromise()
      .then(u => {
        const data = _.get(u, 'body.data', []);
        if (_.isArray(data)) {
          this.empleadoService.empleados = data;
        } else {
          this.empleadoService.empleados = [];
        }
      }).catch(() => {
        this.alertService.alertConfiguration = {
          type: 'danger',
          message: 'Ah ocurrido un error al obtener a los empleados, vuelve a recargar la pagina'
        }
      });
  }
}

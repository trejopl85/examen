import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Subject } from 'rxjs';
import { SprinnerService } from 'src/app/core/services/spinner/spinner.service';

@Component({
  selector: 'app-spinner',
  template: `
    <div *ngIf="isLoading | async" class="overlay">
    <div class="loading">
      <mat-spinner diameter="200"  class="custom-spinner"  [mode]='mode' [value]="value">
      </mat-spinner>
    </div>
    </div>
  `,
  styleUrls: ['./spinner.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent implements OnInit {
  mode: ProgressSpinnerMode = 'indeterminate'
  value = 50;
  isLoading: Subject<boolean> = this.spinnerService.isLoading;
  constructor(private spinnerService: SprinnerService) { }

  ngOnInit() {
  }

}

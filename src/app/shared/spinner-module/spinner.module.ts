import { SpinnerInterceptorService } from './../../core/interceptor/spinner/spinner-interceptor.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SprinnerService } from 'src/app/core/services/spinner/spinner.service';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ], exports: [
    SpinnerComponent
  ], providers: [
    SprinnerService,
    { provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptorService, multi: true },
  ],
  declarations: [SpinnerComponent]
})
export class SpinnerModule { }

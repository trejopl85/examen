import { of, Subject } from 'rxjs';
import { AlertService } from './../../../core/services/alert/alert.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ClrAlert } from '@clr/angular';
type AlertType = 'warning' | 'success' | 'danger';
const SPINNER_TIME_OUT = 2000;
export interface AlertConfiguration {
  message?: string;
  type?: AlertType;
}
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @ViewChild('alert') alert: ClrAlert;
  alertConfiguration: AlertConfiguration
  isLoading: Subject<boolean> = this.alertService.isLoading;
  show = false;
  constructor(private alertService: AlertService) {
    this.alertConfiguration = this.alertService.alertConfiguration;
  }

  ngOnInit() {
    this.isLoading.subscribe(e => {
      if (e) {
        this.alertConfiguration = this.alertService.alertConfiguration;
        this.hide().then(() => {
          this.show = true;
          setTimeout(() => {
            this.show = false;
          }, SPINNER_TIME_OUT);
        });
      }
    })
  }

  private hide(): Promise<boolean> {
    return of(true).toPromise();
  }
  open(): void {
    this.alert.open();
  }

}

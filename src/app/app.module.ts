import { AlertModule } from './shared/alert-module/alert.module';
import { SpinnerModule } from './shared/spinner-module/spinner.module';
import { MaterialComponentModule } from './shared/modules/material-component.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { ListaEmpleadoComponent } from './home/empleado/lista-empleado/lista-empleado.component';
import { EdicionEmpleadoComponent } from './home/empleado/edicion-empleado/edicion-empleado.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetalleEmpleadoComponent } from './home/empleado/detalle-empleado/detalle-empleado.component';
import { EmpleadoPdfComponent } from './home/empleado/empleado-pdf/empleado-pdf.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaEmpleadoComponent,
    EdicionEmpleadoComponent,
    DetalleEmpleadoComponent,
    EmpleadoPdfComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ClarityModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialComponentModule,
    SpinnerModule,
    AlertModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

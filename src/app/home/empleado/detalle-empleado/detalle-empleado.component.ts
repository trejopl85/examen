import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { EmpleadoModel } from 'src/app/shared/model/EmpleadoModel';


@Component({
  selector: 'app-detalle-empleado',
  templateUrl: './detalle-empleado.component.html',
  styleUrls: ['./detalle-empleado.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetalleEmpleadoComponent implements OnInit, OnChanges {
  @Input() empleado: EmpleadoModel;

  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(): void {
  }

}

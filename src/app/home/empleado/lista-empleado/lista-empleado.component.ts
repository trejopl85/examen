import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { EmpleadoModel } from 'src/app/shared/model/EmpleadoModel';
import { EmpleadoService } from './../../../core/services/empleado-service/empleado.service';

export type view = 'CREATE' | 'LIST' | 'EDIT' | 'PDF';
export type tableAction = 'LIST' | 'PDF';
@Component({
  selector: 'app-lista-empleado',
  templateUrl: './lista-empleado.component.html',
  styleUrls: ['./lista-empleado.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListaEmpleadoComponent implements OnInit, OnChanges {
  @Input() empleados: EmpleadoModel[] = [];
  selected: EmpleadoModel[] = [];
  viewType: view = 'LIST';
  @Output() editE: EmpleadoModel;
  @Output() detailE = new EventEmitter<EmpleadoModel>();
  @Input() tableAction: tableAction = 'LIST';
  constructor(private empleadoService: EmpleadoService) { }
  ngOnInit(): void {
    if (this.tableAction === 'LIST') {
      this.empleados = this.empleadoService.empleados;
    }
  }
  ngOnChanges(): void {
    if (this.tableAction === 'LIST') {
      this.empleados = this.empleadoService.empleados;
    }
  }
  edit(): void {
    if (this.selected.length === 1) {
      this.editE = this.selected[0];
      this.viewType = 'EDIT';
    }
  }
  create(): void {
    this.editE = undefined;
    this.viewType = 'EDIT';
  }
  onExportSelected(): void {
    this.viewType = 'PDF';
  }
  onExportAll(): void {
    this.selected = this.empleadoService.empleados;
    this.viewType = 'PDF';
  }
  onDetailOpen(empleado: EmpleadoModel): void {
  }
  getView(e: view) {
    this.selected = [];
    this.viewType = e;
    this.tableAction = 'LIST';
  }
}

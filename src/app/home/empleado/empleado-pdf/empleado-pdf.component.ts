import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas'
import { EmpleadoModel } from 'src/app/shared/model/EmpleadoModel';
import { view } from '../lista-empleado/lista-empleado.component';
@Component({
  selector: 'app-empleado-pdf',
  templateUrl: './empleado-pdf.component.html',
  styleUrls: ['./empleado-pdf.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmpleadoPdfComponent implements OnInit {
  @Input() empleados: EmpleadoModel[] = [];
  @Output() view = new EventEmitter<view>();
  constructor() { }

  ngOnInit(): void {
  }
  public downloadPDF(): void {
    // Extraemos el
    const DATA = document.getElementById('htmlData');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 3
    };
    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
    this.view.emit('LIST');
  }

}

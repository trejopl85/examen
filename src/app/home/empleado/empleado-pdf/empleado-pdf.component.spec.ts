import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoPdfComponent } from './empleado-pdf.component';

describe('EmpleadoPdfComponent', () => {
  let component: EmpleadoPdfComponent;
  let fixture: ComponentFixture<EmpleadoPdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpleadoPdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import * as _ from 'lodash';
import { EmpleadoModel } from 'src/app/shared/model/EmpleadoModel';
import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { REGEX } from 'src/app/core/util/regex-util';
import { OnChanges } from '@angular/core';
import { view } from '../lista-empleado/lista-empleado.component';
import { EmpleadoService } from 'src/app/core/services/empleado-service/empleado.service';
import { BehaviorSubject } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert/alert.service';
@Component({
  selector: 'app-edicion-empleado',
  templateUrl: './edicion-empleado.component.html',
  styleUrls: ['./edicion-empleado.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EdicionEmpleadoComponent implements OnInit, OnChanges {
  @Input() empleado: EmpleadoModel;
  @Output() view = new EventEmitter<view>();
  image = new BehaviorSubject<string>("");
  form: FormGroup;
  constructor(private fb: FormBuilder, private empleadoService: EmpleadoService, private alertService: AlertService) {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.pattern(REGEX.LETTERS_AND_CHARACTERS)]],
      salary: ['', [Validators.required, Validators.pattern(REGEX.NUMBERS_DECIMAL)]],
      age: ['', [Validators.required, Validators.pattern(REGEX.ONLY_NUMERS)]],
      image: ['']
    });
  }

  ngOnInit(): void {
    this
    console.log(this.empleado);
  }

  ngOnChanges(): void {
    this.image.subscribe(e => {
      this.form.get('image').setValue(e);
    });
    if (_.isObject(this.empleado)) {
      const { employee_name, employee_salary, employee_age, profile_image } = this.empleado;
      const img = _.isString(profile_image) ? profile_image : '';
      this.form.get('name').setValue(employee_name);
      this.form.get('salary').setValue(employee_salary);
      this.form.get('age').setValue(employee_age);
      this.image.next(img);
    }
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);

  }
  cancel(): void {
    this.view.emit('LIST');
  }
  confirm(): void {
    const { name, salary, age, image } = this.form.value;
    if (!_.isObject(this.empleado)) {
      const id = Math.max.apply(Math, this.empleadoService.empleados.map((o) => o.id)) + 1;
      this.empleadoService.empleados.push({
        id,
        employee_name: name,
        employee_salary: salary,
        employee_age: age,
        profile_image: image
      });
      this.alertService.alertConfiguration = {
        message: `El empleado ${name} a sido registrado con exito`
      };
    } else {
      const d = this.empleadoService.empleados.filter(e => e.id === this.empleado.id)[0];
      d.employee_name = name;
      d.employee_salary = salary;
      d.employee_age = age;
      d.profile_image = image;
      this.alertService.alertConfiguration = {
        message: `El empleado ${name} a sido actualizado con exito`
      };
    }
    this.alertService.show();
    this.view.emit('LIST');
  }
  // Image Preview
  showPreview(event) {
    if (event.length === 0)
      return;
    const file = (event.target as HTMLInputElement).files[0];

    if (file.name.match('(.*/)*.+\\.(png|jpg|jpeg|PNG|JPG|JPEG)$') == null) {
      this.image.next('');
      this.form.get('image').setValue('');
      this.form.get('image').setErrors({
        pattern: true
      });
      return;
    }

    this.form.patchValue({
      image: file
    });
    this.form.get('image').updateValueAndValidity()

    // File Preview
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.image.next(reader.result as string);
    }
  }
  control(name: string): FormControl {
    return this.form.get(name) as FormControl;
  }

}
